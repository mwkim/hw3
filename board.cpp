//Andrew Doan, Myung Kim
// board.cpp

#include <cstring> 
#include <iostream>  
#include <cstdio>
#include "board.h"


void Board::reset(void) {
    memset(board, 0, sizeof(unsigned char) * 67);
}

void Board::print_square(int square)
{
        putchar(FILE2CHAR(F(square)));
        putchar(RANK2CHAR(R(square)));
}

void Board::print_board(int ply)
{
    int file, rank;
	
    for (rank=RANK_8; rank>=RANK_1; rank--) {
        std::cout << 1+rank << ' ';
        for (file=FILE_A; file<=FILE_H; file++) {
                putchar(' ');
                putchar(PIECE2CHAR(board[SQ(file,rank)]));
        }
        putchar('\n');
    }
    std::cout   << "   a b c d e f g h\n"
				<< 1+ply/2 << ". "
				<< ((~ply & 1) ? "White" : "Black")
				<< " to move. " 
				<< (board[CASTLE] & CASTLE_WHITE_KING ? "K" : "")
				<< (board[CASTLE] & CASTLE_WHITE_QUEEN ? "Q" : "")
				<< (board[CASTLE] & CASTLE_BLACK_KING ? "k" : "")
				<< (board[CASTLE] & CASTLE_BLACK_QUEEN ? "q " : " ");
    
    if (board[EP]) print_square(board[EP]);
    putchar('\n');
}   

void Board::print_move_long(int move)
{
	print_square(FR(move));
	print_square(TO(move));
	if (move >= SPECIAL) {
		if ((board[FR(move)] == WHITE_PAWN && R(TO(move)) == RANK_8)
			||  (board[FR(move)] == BLACK_PAWN && R(TO(move)) == RANK_1)) {
		         putchar('=');
             putchar("QRBN"[move >> 13]);
    }
	}
}

