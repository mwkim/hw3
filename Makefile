# Andrew Doan, Myung Kim
#	Makefile for mscp
#

CC=g++
CFLAGS=-Wall -O2 -ansi -pedantic

TARGET=mscp
SOURCE=mscp.cpp board.cpp

main:	mscp.cpp board.cpp board.h
	$(CC) $(CFLAGS) -o $(TARGET) $(SOURCE)

clean:
	rm -f $(TARGET)

